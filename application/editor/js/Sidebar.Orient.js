import * as THREE from 'three';

import { UIPanel, UIRow, UIInput, UIButton, UITextArea, UIText, UINumber, UIHorizontalRule } from './libs/ui.js';

function SidebarOrient( editor ) {
  const strings = editor.strings;

	const signals = editor.signals;

	const container = new UIPanel();
	container.setBorderTop( '0' );
	container.setPaddingTop( '20px' );
	container.setDisplay( 'none' );


	//----------------------------------------------------//
	//START OF UI ----------------------------------------//
	//----------------------------------------------------//

	// hr
	container.add(new UIHorizontalRule());

	//Orientation Goals Title
	const orientPerformanceRow = new UIRow();
	orientPerformanceRow.add( new UIText( strings.getKey( 'sidebar/orient/performance' ) ).setWidth( '300px' ) ); //
	container.add( orientPerformanceRow );

	// hr
	container.add(new UIHorizontalRule());

	// Number of directions
	const orientDirectionsRow = new UIRow();
	const orientDirections = new UINumber().setPrecision( 0 ).setStep(50).setNudge(20).setWidth( '100px' ).setRange(20, 2000).onChange( update ).setValue(60);

	orientDirectionsRow.add( new UIText( strings.getKey( 'sidebar/orient/directions' ) ).setWidth( '200px' ) );
	orientDirectionsRow.add( orientDirections );

	container.add( orientDirectionsRow );

	// Buffer Size

	const orientBufferRow = new UIRow();
	const orientBuffer = new UINumber().setPrecision( 0 ).setStep(30).setNudge(16).setWidth( '100px' ).setRange(32, 1024).onChange( update ).setValue(32);

	orientBufferRow.add( new UIText( strings.getKey( 'sidebar/orient/buffer' ) ).setWidth( '200px' ) );
	orientBufferRow.add( orientBuffer );

	container.add( orientBufferRow );


	// hr
	container.add(new UIHorizontalRule());

	//Orientation Goals Title
	const orientGoalsRow = new UIRow();
	orientGoalsRow.add( new UIText( strings.getKey( 'sidebar/orient/goals' ) ).setWidth( '300px' ) ); //
	container.add( orientGoalsRow );

	// hr
	container.add(new UIHorizontalRule());

	// Height TIZIANO (precision, range, set value)

	const orientHeightRow = new UIRow();
	const orientHeight = new UINumber().setPrecision(0).setStep(8).setNudge(10).setWidth( '100px' ).setRange(0,100).onChange( update ).setValue(50);

	orientHeightRow.add( new UIText( strings.getKey( 'sidebar/orient/height' ) ).setWidth( '200px' ) );
	orientHeightRow.add( orientHeight );

	container.add( orientHeightRow );

	// Shadow TIZIANO (precision, range, set value)

	const orientShadowRow = new UIRow();
	const orientShadow = new UINumber().setPrecision(0).setStep(8).setNudge(10).setNudge(10).setWidth( '100px' ).setRange(0,100).onChange( update ).setValue(50);

	orientShadowRow.add( new UIText( strings.getKey( 'sidebar/orient/shadow' ) ).setWidth( '200px' ) );
	orientShadowRow.add( orientShadow );

	container.add( orientShadowRow );

	// Contact TIZIANO (precision, range, set value)

	const orientContactRow = new UIRow();
	const orientContact = new UINumber().setPrecision(0).setStep(8).setNudge(10).setWidth( '100px' ).setRange(0,100).onChange( update ).setValue(50);

	orientContactRow.add( new UIText( strings.getKey( 'sidebar/orient/contact' ) ).setWidth( '200px' ) );
	orientContactRow.add( orientContact );

	container.add( orientContactRow );

	// Volume TIZIANO (precision, range, set value)

	const orientVolumeRow = new UIRow();
	const orientVolume = new UINumber().setPrecision(0).setStep(8).setNudge(10).setWidth( '100px' ).setRange(0,100).onChange( update ).setValue(50);

	orientVolumeRow.add( new UIText( strings.getKey( 'sidebar/orient/volume' ) ).setWidth( '200px' ) );
	orientVolumeRow.add( orientVolume );

	container.add( orientVolumeRow );

	// hr
	container.add(new UIHorizontalRule());

	//Avoid specific directions Title
	const orientAvoidRow = new UIRow();
	orientAvoidRow.add( new UIText( strings.getKey( 'sidebar/orient/avoid' ) ).setWidth( '300px' ) ); //
	container.add( orientAvoidRow );



	// hr
	container.add(new UIHorizontalRule());

	// Critical

	const orientCriticalRow = new UIRow();
	const orientCritical = new UINumber().setPrecision(0).setStep(16).setNudge(10).setWidth( '100px' ).setRange(0, 500).onChange( update ).setValue(50);

	orientCriticalRow.add( new UIText( strings.getKey( 'sidebar/orient/critical' ) ).setWidth( '200px' ) );
	orientCriticalRow.add( orientCritical );

	container.add( orientCriticalRow );

	// Cone

	const orientConeRow = new UIRow();
	const orientCone = new UINumber().setPrecision( 1 ).setStep(7).setNudge(0.5).setWidth( '100px' ).setRange(0, 90).onChange( update ).setValue(3.0);

	orientConeRow.add( new UIText( strings.getKey( 'sidebar/orient/cone' ) ).setWidth( '200px' ) );
	orientConeRow.add( orientCone );

	container.add( orientConeRow );

	// Forbidden

	const orientForbiddenRow = new UIRow();
	orientForbiddenRow.add( new UIText( strings.getKey( 'sidebar/orient/forbidden' ) ).setWidth( '200px' ));
	const orientForbidden = new UITextArea().onChange( update ).setValue('[]');
	orientForbidden.dom.setAttribute( 'rows', 1);
	orientForbidden.dom.setAttribute( 'style', 'height: 32px; width: 100px; resize: none; vertical-align: middle;' );
	const orientForbiddenAdd = new UIButton(strings.getKey('sidebar/orient/forbiddenAdd')).setWidth( '100px').onClick(() => {
		const model = editor.scene.children[editor.config.getKey('scene/modelRef')];
		if ( model !== undefined ) {
			
			let vertDirection = new THREE.Vector3();
			//.getWorldDirection restituisce la direzione dell'asse Z locale nella world coordinate system
			model.getWorldDirection(vertDirection);
			console.log("vertDirection");
			console.log(vertDirection);

			//Questa direzione va invertita come segue (-x,-y,z)
			//trovato con test sperimentali

			let tmp =
				'[' +
				-(vertDirection.x) +
				', ' +
				-(vertDirection.y) +
				', ' +
				vertDirection.z +
				']';

			console.log("tmp");
			console.log(tmp);
			
			//let tmp =
			//	'[' +
			//	(model.quaternion._x).toFixed(2) +
			//	', ' +
			//	model.quaternion._y.toFixed(2) +
			//	', ' +
			//	model.quaternion._z.toFixed(2) +
			//	']';
	
			const textArea = document.querySelector('#forbidden textarea');
			if ( textArea.value === '[]' ||  textArea.value === '' ) {
				textArea.value = '[' + tmp + ']';
			} else {
				textArea.value = textArea.value.slice(0, textArea.value.length - 1) + ', ' + tmp + ']';
			}
		}
	});
	orientForbiddenAdd.dom.setAttribute('style', 'margin: 0px 10px');
	orientForbiddenRow.add( orientForbidden );
	orientForbiddenRow.add( orientForbiddenAdd );
	container.add( orientForbiddenRow );

	//----------------------------------------------------//
	//END OF UI ------------------------------------------//
	//----------------------------------------------------//


	function update() {
		const object = editor.selected;

		// All the scene updates in here

	}

	function updateRows() {

		const properties = {
			'height' : orientHeightRow,
			'shadow' : orientShadowRow,
			'contact' : orientContactRow,
			'volume' : orientVolumeRow,
			'critical' : orientCriticalRow,
			'cone' : orientConeRow,
			'forbidden' : orientForbiddenRow,
			'buffer' :  orientBufferRow,
			'directions' :  orientDirectionsRow,
		};

		for ( const property in properties ) {

			const uiElement = properties[ property ];
			uiElement.setDisplay( 'block' );
			uiElement.setId(property);

			if ( Array.isArray( uiElement ) === true ) {

				for ( let i = 0; i < uiElement.length; i ++ ) {

					uiElement[ i ].setDisplay( 'block' );

				}

			}
		}
	}

	updateRows();
	container.setDisplay( 'block' );

  return container;
}

export { SidebarOrient };